﻿using System;
using System.Collections;
using UnityEngine;

public class TreeRessource : Ressource {
    public float dropEffectDelay = 1;
    public int fallenHitpoints = 10;
    GameObject currentParticleEffect;
    bool hasFallen;

    public void Start() {
        var models = gameObject.transform.FindChild("Models");
        var iSelectedChild = UnityEngine.Random.Range(0, models.childCount);
        for(int i = 0; i < models.childCount; i++) {
            models.GetChild(i).gameObject.SetActive(i == iSelectedChild);
        }
    }

    internal override void StartHarvestEffects() {
        base.StartHarvestEffects();
        currentParticleEffect = gameObject.transform.FindChild("Harvesting").gameObject;
        currentParticleEffect.SetActive(true);
    }

    internal override void StopHarvestEffects() {
        base.StopHarvestEffects();
        if(currentParticleEffect != null) {
            currentParticleEffect.SetActive(false);
            currentParticleEffect = null;
        }
    }

    protected override void HarvestActionComplete(BasicWorker worker) {
        ShowDropParticles();

        Beehive bh = gameObject.transform.GetComponentInChildren<Beehive>();
        if(bh != null)
            bh.Fall();
        var body = gameObject.transform.GetComponent<Rigidbody>();
        body.useGravity = true;
        body.isKinematic = false;
        body.constraints = RigidbodyConstraints.None;
        //todo rotate random or rotate based on player position
        highlight.SetActive(false);
        var models = gameObject.transform.FindChild("Models");

        GameObject selectedModel = null;
        for(int i = 0; i < models.childCount; i++) {
            var child = models.GetChild(i);
            if(child.gameObject.activeSelf) {
                selectedModel = child.gameObject;
            }
        }

        var stump = selectedModel.transform.FindChild("Stump");
        if(stump != null) {
            stump.SetParent(null, true);
        }

        body.AddTorque(worker.transform.rotation * new Vector3(0, 0, 150));

        //gameObject.transform.eulerAngles = new Vector3(
        //    gameObject.transform.eulerAngles.x,
        //    gameObject.transform.eulerAngles.y,
        //    gameObject.transform.eulerAngles.z + 10
        //);

        base.HarvestActionComplete(worker);
        if(!hasFallen) {
            hasFallen = true;
            curHitpoints = fallenHitpoints;
            worker.SetResourceToCollect(this);
        } else {
            base.HarvestCompleteSpawnLoot();
        }
    }

    private void ShowDropParticles() {
        StartCoroutine(ShowDropParticlesAfterDelay());
    }

    private IEnumerator ShowDropParticlesAfterDelay() {
        yield return new WaitForSeconds(dropEffectDelay);
        currentParticleEffect = gameObject.transform.FindChild("Drop").gameObject;
        currentParticleEffect.SetActive(true);
    }
}