﻿using System.Collections;
using UnityEngine;

public class Beehive : MonoBehaviour
{
    public GameObject BeeSwarm;

    public GameObject drop;
    public int dropMaxCount;
    public int dropMinCount;

    public AudioClip harvestSound;
    public float harvestSoundVolume;

    public void Fall()
    {
        transform.SetParent(GetEnvironmentInteractableBase(transform.parent));
        Rigidbody rb = GetComponent<Rigidbody>();
        if (!rb.useGravity)
        {
            rb.useGravity = true;
            rb.isKinematic = false;
        }

        Destroy(GetComponent<ParticleSystem>());
        Destroy(this);
        BeehiveRessource res = gameObject.AddComponent<BeehiveRessource>();
        res.drop = drop;
        res.dropMaxCount = dropMaxCount;
        res.dropMinCount = dropMinCount;
        res.harvestSound = harvestSound;
        res.harvestSoundVolume = harvestSoundVolume;

        Instantiate(BeeSwarm, transform.position, Quaternion.identity);
    }

    Transform GetEnvironmentInteractableBase(Transform trans)
    {
        if(trans == null || trans.name == "EnvironmentInteractables")
            return trans;
        return GetEnvironmentInteractableBase(trans.parent);
    }
}