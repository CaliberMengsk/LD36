﻿using UnityEngine;
using System.Collections;

public class Ghost : MonoBehaviour
{
    public EnvironmentSpawner spawner;

    Renderer renderer;

    public float swapTime = 20;
    float swapTimer = 0;

    public float invisibilityTime = 0.75f;
    float invisibility = 0;

    public float scareDistance = 7;
    public float scareTime = 5;

    void Start()
    {
        renderer = GetComponent<Renderer>();
        Swap();
    }

    void Update()
    {
        if (swapTimer > swapTime)
        {
            swapTimer = 0;
            Swap();
        }
        swapTimer += Time.deltaTime;

        if (invisibility > 0)
            invisibility -= Time.deltaTime;
        else if (invisibility < 0) invisibility = 0;
        if (renderer.enabled && invisibility == 0)
            renderer.enabled = false;
    }

    void Swap()
    {
        int i = 0;
        int swapTarget = Random.Range(0, spawner.transform.childCount);
        foreach (Transform child in spawner.transform)
        {
            if (i == swapTarget)
            {
                transform.position = child.position + (new Vector3(0, 5, 0));
                transform.parent = child;
                renderer.enabled = false;
                return;
            }
            ++i;
        }
    }

    public Vector3 Boo(Vector3 position)
    {
        renderer.enabled = true;
        invisibility = invisibilityTime;
        transform.rotation = Quaternion.LookRotation((transform.position - position).normalized);

        NavMeshHit newDestination;
        NavMesh.SamplePosition(transform.position + (new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) * scareDistance), out newDestination, 10, 1);
        return newDestination.position;
    }
}
