﻿using UnityEngine;
using System.Collections.Generic;

public class BiomeController : MonoBehaviour {
    public GameObject baseMesh;
    public bool isPathable = true;
    public float tileHeightOffset = 0;
    public float objectHeightOffset = 0;
    public GameObject[] objectsToSpawn;
    public Vector2[] chanceRange;

    public static readonly float scaleX = 2.0f / 6.0f;
    public static readonly float scaleY = 0.6f;
    public static readonly Vector3 borderOffset = new Vector3(0.0f, -0.25f, 0.0f);

    Vector3 computeCoordinate(int gridX, int gridY)
    {
        return new Vector3(gridX * scaleX, tileHeightOffset, gridY * scaleY);
    }

    public void GenerateTileObjects(int seed, int x, int y, CMChunk chunk)
    {
        int baseX = x * 6;
        int baseY = y;
        if ((y & 1) == 1)
        {
            baseX += 3;
        }
        
        Vector3 translation = computeCoordinate(baseX, baseY);

        // Optimization: no need to compute the same random multiple times
        Random.seed = seed * x * y;
        float rangeValue = Random.Range(0f, 1f);
        if (!isPathable)
        {
            GameObject pathObject = new GameObject();
            pathObject.name = "Obsticale:" + x + "," + y;
            NavMeshObstacle nOb = pathObject.AddComponent<NavMeshObstacle>();
            nOb.size = new Vector3(1.2f, 10, 1.2f);
            nOb.center = Vector3.zero;
            nOb.carving = true;
            pathObject.transform.parent = chunk.transform;
            pathObject.transform.position = translation;
        }
        for (int i = 0; i < objectsToSpawn.Length; i++)
        {
            if (rangeValue > chanceRange[i].x && rangeValue < chanceRange[i].y)
            {
                GameObject child = Instantiate(objectsToSpawn[i]);
                child.transform.parent = chunk.transform;
                child.transform.localPosition = new Vector3(Random.Range(-.5f, .5f), objectHeightOffset, Random.Range(-.5f, .5f)) + translation;
                child.transform.eulerAngles = new Vector3(child.transform.eulerAngles.x, Random.Range(-180, 180), child.transform.eulerAngles.z);
                break;
            }
        }

    }

    public static readonly int[] tileNeighborsXEven = { -1, 0, 0, 0, 0, -1 };
    public static readonly int[] tileNeighborsXOdd  = {  0,  0,  1, 1,  0,  0 };
    public static readonly int[] tileNeighborsY     = { -1, -2, -1,  1, 2,  1 };

    public void GenerateTiles(int x0, int y0, CMChunk chunk,
        int chunkSize, int myID, List<BiomeController> otherBiomes)
    {
        int countTiles = 0;
        int countBorders = 0;
        for (int tileX = 0; tileX < chunkSize; ++tileX)
        {
            for (int tileY = 0; tileY < chunkSize; ++tileY)
            {
                bool odd = ((tileY & 1) == 1);
                if (chunk.tileBiomeID[tileX, tileY] == myID)
                {
                    ++countTiles;
                    if (tileHeightOffset >= 0)
                    {
                        // look at all 6 neighbors
                        for (int t2 = 0; t2 < 6; ++t2)
                        {
                            int t2X = tileX + (odd ? tileNeighborsXOdd[t2] : tileNeighborsXEven[t2]);
                            int t2Y = tileY + tileNeighborsY[t2];
                            bool border = false;
                            if (t2X < 0 || t2X >= chunkSize ||
                                t2Y < 0 || t2Y >= chunkSize)
                            {
                                border = true; // out of this chunk, add a border just in case
                            }
                            else
                            {
                                int bid2 = chunk.tileBiomeID[t2X, t2Y];
                                if (bid2 != myID && otherBiomes[bid2].tileHeightOffset < otherBiomes[myID].tileHeightOffset)
                                {
                                    border = true;
                                }
                            }
                            if (border)
                            {
                                ++countBorders;
                            }
                        }
                    }
                }
            }
        }

        GameObject baseReturn = Instantiate(baseMesh);
        var meshFilter = baseReturn.GetComponent<MeshFilter>();
        var mesh = new Mesh();

        int vCount = 6 * countTiles + 4 * countBorders;
        int tCount = 4 * countTiles + 2 * countBorders;
        Vector3[] vertices = new Vector3[vCount];
        Vector3[] normals  = new Vector3[vCount];
        Vector2[] uv = new Vector2[vCount];
        int[] tri = new int[3 * tCount];

        int v0 = 0;
        int i0 = 0;

        for (int tileX = 0; tileX < chunkSize; ++tileX)
        {
            for (int tileY = 0; tileY < chunkSize; ++tileY)
            {
                bool odd = ((tileY & 1) == 1);
                if (chunk.tileBiomeID[tileX, tileY] == myID)
                {
                    int baseX = (x0 + tileX) * 6;
                    int baseY = (y0 + tileY);
                    if ((baseY & 1) == 1)
                    {
                        baseX += 3;
                    }

                    // add tile
                    int tileV0 = v0; // to remember where this tile vertices are

                    vertices[v0 + 0] = computeCoordinate(baseX - 2, baseY + 0);
                    vertices[v0 + 1] = computeCoordinate(baseX - 1, baseY - 1);
                    vertices[v0 + 2] = computeCoordinate(baseX + 1, baseY - 1);
                    vertices[v0 + 3] = computeCoordinate(baseX + 2, baseY + 0);
                    vertices[v0 + 4] = computeCoordinate(baseX + 1, baseY + 1);
                    vertices[v0 + 5] = computeCoordinate(baseX - 1, baseY + 1);

                    normals[v0 + 0] = Vector3.up;
                    normals[v0 + 1] = Vector3.up;
                    normals[v0 + 2] = Vector3.up;
                    normals[v0 + 3] = Vector3.up;
                    normals[v0 + 4] = Vector3.up;
                    normals[v0 + 5] = Vector3.up;

                    tri[i0 +  0] = v0 + 0;
                    tri[i0 +  1] = v0 + 5;
                    tri[i0 +  2] = v0 + 1;
                    tri[i0 +  3] = v0 + 1;
                    tri[i0 +  4] = v0 + 5;
                    tri[i0 +  5] = v0 + 2;
                    tri[i0 +  6] = v0 + 5;
                    tri[i0 +  7] = v0 + 4;
                    tri[i0 +  8] = v0 + 2;
                    tri[i0 +  9] = v0 + 2;
                    tri[i0 + 10] = v0 + 4;
                    tri[i0 + 11] = v0 + 3;

                    v0 += 6;
                    i0 += 12;

                    if (tileHeightOffset >= 0)
                    {
                        // look at all 6 neighbors
                        for (int t2 = 0; t2 < 6; ++t2)
                        {
                            int t2X = tileX + (odd ? tileNeighborsXOdd[t2] : tileNeighborsXEven[t2]);
                            int t2Y = tileY + tileNeighborsY[t2];
                            bool border = false;
                            if (t2X < 0 || t2X >= chunkSize ||
                                t2Y < 0 || t2Y >= chunkSize)
                            {
                                border = true; // out of this chunk, add a border just in case
                            }
                            else
                            {
                                int bid2 = chunk.tileBiomeID[t2X, t2Y];
                                if (bid2 != myID && otherBiomes[bid2].tileHeightOffset < otherBiomes[myID].tileHeightOffset)
                                {
                                    border = true;
                                }
                            }
                            if (border)
                            {
                                int borderVA = tileV0 + t2; // index of the first vertex of this edge
                                int borderVB = tileV0 + ((t2+1)%6); // index of the second vertex of this edge

                                vertices[v0 + 0] = vertices[borderVA];
                                vertices[v0 + 1] = vertices[borderVB];
                                vertices[v0 + 2] = vertices[borderVB] + borderOffset;
                                vertices[v0 + 3] = vertices[borderVA] + borderOffset;
                                Vector3 n = Vector3.Cross(vertices[v0 + 1] - vertices[v0 + 0], borderOffset);
                                n.Normalize();
                                normals[v0 + 0] = n;
                                normals[v0 + 1] = n;
                                normals[v0 + 2] = n;
                                normals[v0 + 3] = n;

                                tri[i0 + 0] = v0 + 0;
                                tri[i0 + 1] = v0 + 1;
                                tri[i0 + 2] = v0 + 2;
                                tri[i0 + 3] = v0 + 0;
                                tri[i0 + 4] = v0 + 2;
                                tri[i0 + 5] = v0 + 3;

                                v0 += 4;
                                i0 += 6;
                            }
                        }
                    }
                }
            }
        }


        mesh.vertices = vertices;
        mesh.triangles = tri;
        mesh.normals = normals;
        for(int i = 0; i < uv.Length; ++i)
            uv[i] = new Vector2(vertices[i].x*0.1f, vertices[i].z*0.1f + vertices[i].y*0.1f);
        mesh.uv = uv;
        meshFilter.mesh = mesh;
        baseReturn.transform.position = new Vector3(0,0,0);
        baseReturn.transform.rotation = Quaternion.identity;
        baseReturn.transform.parent = chunk.transform;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
