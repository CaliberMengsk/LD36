﻿using UnityEngine;
using System.Collections;

public class FireLifeBubble : MonoBehaviour
{

    const string fire = "{0} energy left";
    private ChatBubble bubble;
    public CampfireFueled campFire;
    private int currentFire;

    void Awake()
    {
        ChatBubbleManager.CreateChatBubble(transform, "fire", Vector2.zero, Color.black);
        campFire = gameObject.GetComponent<CampfireFueled>();
        currentFire = campFire.RemainingFuel;
    }

    void Update()
    {
        if (currentFire == campFire.RemainingFuel) return;
        currentFire = campFire.RemainingFuel;

        bubble.label.text = string.Format(fire, currentFire);

    }

}