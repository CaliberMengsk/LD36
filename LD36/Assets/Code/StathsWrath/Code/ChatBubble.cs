﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class ChatBubble : MonoBehaviour
{
    private RectTransform rTransform;
    public Transform target;
    public Vector3 positionModifier;
    public Text label;
    public float lifetime;
    private bool counting;

    private void Awake()
    {
        rTransform = transform as RectTransform;
    }

    public void StartFinalCountdown()
    {
        StartCoroutine(QueueDestruction());
    }

    private IEnumerator QueueDestruction()
    {
        if (counting) yield break;
        counting = true;

        while (lifetime >= 0)
        {
            lifetime -= Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        ChatBubbleManager.RemoveBubble(this);
        GameObject.Destroy(this.gameObject);
    }

    private void Update()
    {
        rTransform.position = Camera.main.WorldToScreenPoint(target.position) + positionModifier;
    }
}