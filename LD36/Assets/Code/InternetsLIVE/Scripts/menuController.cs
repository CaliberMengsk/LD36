﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menuController : MonoBehaviour {

    public void Credits() {
        SceneManager.LoadScene("Credits");
    }

    public void ExitGame() {
        //only if executable
        Application.Quit();
    }

    public void PlayIntro() {
        SceneManager.LoadScene("Intro");
    }

    //Author InternetsLIVE
    public void StartGame() {
        SceneManager.LoadScene("Main");
    }
}