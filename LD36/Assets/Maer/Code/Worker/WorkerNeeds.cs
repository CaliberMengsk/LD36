﻿using UnityEngine;
using System.Collections;

public class WorkerNeeds : MonoBehaviour {

    //Just a basic template

    [Header("temperature")]
    public int curTemperature; //Cur temp the player can have
    public int freezingTemp = 5; //When does the player start to freeze
    public int burningTemp = 40; //when does the player start to get to hot

    [Header("Food")]
    public float curSaturation = 100; //Low value = we have hunger, high value = everything is ok
    public int maxSaturation = 100; //How saturated we can get

    [Header("Chatter")]
    public Chatter chatter; //For fun at the moment

    [HideInInspector]
    public BasicWorker myWorker;

    #region Events
    void OnEnable()
    {
        TickManager.workTick += GetTick;
        curSaturation = maxSaturation;
    }

    void OnDisable()
    {
        TickManager.workTick -= GetTick;
    }

    public void GetTick()
    {
        chatter.FunMode();
    }
    #endregion

    #region Temp
    public void SetTemperature(int _temp)
    {
        curTemperature = _temp;

        if (curTemperature <= freezingTemp)
        {
            if(chatter.CanWeChat()) 
            {
                chatter.AddText("Its so cold, why do you do this to me Hardly?", Color.red);
                NeedsEvents.WorkerFreezing(myWorker);
            }

        }

        if (curTemperature >= burningTemp)
        {
            if (chatter.CanWeChat())
            {
                chatter.AddText("Its so hot, WTF my skin is melting. What is happening?", Color.red);
                NeedsEvents.WorkerBurning(myWorker);
            }
        }
    }
    #endregion

    #region Hunger
    /// <summary>
    /// Gets hungry every tick -1 each tick
    /// </summary>
    public void GetHungry()
    {
        curSaturation--;

        if(curSaturation <= 0)
        {
            chatter.AddText("I am so hungy, why do i need to starve?", Color.red);
            NeedsEvents.WorkerStarving(myWorker);
        }
    }

    /// <summary>
    /// Eat to not be hungry anymore
    /// </summary>
    /// <param name="_value"></param>
    public void Eat(int _value)
    {
        curSaturation += _value;

        if (curSaturation > maxSaturation)
            curSaturation = maxSaturation;
    }
    #endregion
}
