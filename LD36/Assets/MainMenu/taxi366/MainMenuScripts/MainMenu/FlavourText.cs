﻿using UnityEngine;
using UnityEngine.UI;

public class FlavourText : MonoBehaviour
{
    public string[] possibleText;
    private bool makeSmaller;

    void Start()
    {
        GetComponent<Text>().text = possibleText[Random.Range(0, possibleText.Length)];
    }

    void Update()
    {
        ShrinkAndGrowText();
    }

    void ShrinkAndGrowText()
    {
        if (!makeSmaller)
        {
            if (GetComponent<RectTransform>().localScale.x < 1)
            {
                GetComponent<RectTransform>().localScale = GetComponent<RectTransform>().localScale + (new Vector3(1, 1, 1) * Time.deltaTime);
            }
            else
            {
                makeSmaller = true;
            }
        }
        else
        {
            if (GetComponent<RectTransform>().localScale.x > 0.5)
            {
                GetComponent<RectTransform>().localScale = GetComponent<RectTransform>().localScale - (new Vector3(1, 1, 1) * Time.deltaTime);
            }
            else
            {
                makeSmaller = false;
            }
        }
    }
}