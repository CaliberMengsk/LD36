﻿using UnityEngine;
using System.Collections.Generic;

namespace Credits
{
    public class CreditsManager : MonoBehaviour
    {
        //a List of all of the possible spawn points
        public List<SpawnpointScript> spawnPoints;

        //a list of all of the people that contributed;
        public List<CustomVariables.Credits> allCredits;

        void Start()
        {
            for (int i = 0; i < allCredits.Count; i++)
            {
                int pointToUse = Random.Range(0, spawnPoints.Count);

                spawnPoints[pointToUse].thisPointsCredits.Add(allCredits[i]);
            }
        }

        //you need something to trigger this
        public void Apply()
        {
            CurrentCredits.credits = allCredits;
        }
    }
}