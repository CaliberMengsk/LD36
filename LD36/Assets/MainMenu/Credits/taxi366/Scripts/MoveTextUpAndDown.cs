﻿using UnityEngine;
using UnityEngine.UI;

namespace Credits
{
    public class MoveTextUpAndDown : MonoBehaviour
    {
        /*
        This just moves the text up and down so it looks like it is comming out if the trees
        */

        //the point that the text is spawned at;
        private Vector3 startingPosition;
        //the point that spawned the text
        [System.NonSerialized]
        public SpawnpointScript mySpawnpoint;

        //the 2 text components on the prefab i am adding then here becau it is more efficent than finding them i believe
        public Text nameText;
        public Text roleText;

        [System.NonSerialized]
        public CustomVariables.Credits myCredit;

        private bool shouldMoveDown;

        void Start()
        {
            startingPosition = gameObject.transform.position;
            shouldMoveDown = false;

            //sets the name and role text from the thing
            nameText.text = myCredit.name;
            roleText.text = myCredit.role;
        }

        void Update()
        {
            MoveText();
        }

        void MoveText()
        {
            //Sorry this is a mess HD dont hurt me Pls <3

            if (!shouldMoveDown)
            {
                if (startingPosition.y + 330 > gameObject.transform.position.y)
                {
                    gameObject.transform.position += new Vector3(0, 100, 0) * Time.deltaTime;
                }
                else
                {
                    shouldMoveDown = true;
                }
            }
            else
            {
                if (startingPosition.y > gameObject.transform.position.y)
                {
                    mySpawnpoint.canSpawnNewText = true;
                    mySpawnpoint.SpawnDelay();
                    Destroy(gameObject);
                }
                else
                {
                    gameObject.transform.position -= new Vector3(0, 100, 0) * Time.deltaTime;
                }
            }
        }
    }
}