﻿using UnityEngine;
using System.Collections.Generic;

namespace Credits
{
    public class SpawnpointScript : MonoBehaviour
    {
        //dont add credits here it is a bad idea
        public List<CustomVariables.Credits> thisPointsCredits;
        public GameObject objectToSpawn;

        private int spawnCount;

        private float randomSpawndelay;

        [System.NonSerialized]
        public bool canSpawnNewText = true;

        void Awake()
        {
            thisPointsCredits.Clear();
            SpawnDelay();
        }

        void Update()
        {
            //wont try to make a thing if it was not assigned any credits data or if it has already run through all of its things
            if(thisPointsCredits.Count > 0 && thisPointsCredits.Count - 1 >= spawnCount)
            {
                if(Time.time > randomSpawndelay)
                {
                    //makes sure that there is not already a text object moving
                    if (canSpawnNewText)
                    {
                        SpawnObject();
                    }
                }
            }
        }

        void SpawnObject()
        {
            canSpawnNewText = false;
            GameObject spawnedObject = (GameObject)Instantiate(objectToSpawn, transform.position, transform.rotation);
            //                              ^ Why do i need to cast this? Like seriously...It is just annoying

            //sets the required infomation in the spawned object
            spawnedObject.GetComponent<MoveTextUpAndDown>().mySpawnpoint = this;
            spawnedObject.GetComponent<MoveTextUpAndDown>().myCredit.name = thisPointsCredits[spawnCount].name;
            spawnedObject.GetComponent<MoveTextUpAndDown>().myCredit.role = thisPointsCredits[spawnCount].role;

            spawnCount++;
        }

        //this exists becausei like using random numbers and to that all of the text does not happen at the same time this is also called from the MoveTextUpAndDown script
        public void SpawnDelay()
        {
            randomSpawndelay = Time.time + Random.Range(0f, 10f);
        }
    }
}